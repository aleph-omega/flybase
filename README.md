
# References
Flybase is a huge database and only some gff3 files have been filtered for trial use from the [Flybase FTP archive](ftp://ftp.flybase.net/releases/current/). For the latest files, download the flybase archives: 
- http://flybase.org/static_pages/downloads/bulkdata7.html
- http://flybase.org/static_pages/docs/datafiles.html


### Licence and Copyrights
- Copyrights are owned by the respective Org archive maintainers, as per the Licence they chose to apply on their data. For exact details, visit the respective Org.


### Mirrors
- https://gitlab.com/aleph-omega/flybase/tree/master
- https://git.cloudhost.io/aleph-omega/flybase/tree/master  
